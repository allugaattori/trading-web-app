package com.group14.tradeapp.modeltest;

import com.group14.tradeapp.model.ShareExchange;
import com.group14.tradeapp.model.enums.ExchangeType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class ShareExchangeTest {

    @Test
    public void createNewShareExchangeSuccessfully() {
        ShareExchange shareExchange = new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 200, 2, Instant.now());
        assertNotNull(shareExchange.getId());
        assertEquals(shareExchange.getExchangeType(), ExchangeType.BID);
        assertEquals(shareExchange.getPrice(), 200);
        assertEquals(shareExchange.getQuantity(), 2);
        assertNotNull(shareExchange.getTimestamp());
    }

    @Test
    public void updateShareExchangeSuccessfully() {
        ShareExchange shareExchange = new ShareExchange(UUID.randomUUID(), ExchangeType.OFFER, 60, 100, Instant.now());
        assertEquals(shareExchange.getQuantity(), 100);
        shareExchange.setQuantity(200);
        assertEquals(shareExchange.getQuantity(), 200);
    }

}

