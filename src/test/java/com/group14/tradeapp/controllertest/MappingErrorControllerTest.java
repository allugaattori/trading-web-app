package com.group14.tradeapp.controllertest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class MappingErrorControllerTest {

    @Autowired
    private MockMvc mockMvc;
    private final String localUrl = "http://localhost:8080/error";

    @Test
    public void getErrorMappingAndFetchCorrectErrorMessage() throws Exception {
        this.mockMvc.perform(get(localUrl)).andDo(print()).andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Whoops")));
    }

}