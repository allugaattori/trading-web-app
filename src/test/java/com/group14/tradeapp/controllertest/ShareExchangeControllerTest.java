package com.group14.tradeapp.controllertest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.group14.tradeapp.model.ShareExchange;
import com.group14.tradeapp.model.enums.ExchangeType;
import com.group14.tradeapp.service.ShareService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.UUID;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class ShareExchangeControllerTest {

    @Autowired
    private ShareService service;
    @Autowired
    private MockMvc mockMvc;
    private final String localUrl = "http://localhost:8080/tradeapp/v1/";

    @Test
    public void getAppInformationSuccessfully() throws Exception {
        this.mockMvc.perform(get(localUrl)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Tradeapp")));
    }

    @Test
    public void postNewBidSuccessfully() throws Exception {
        double stockPrice = service.getLatestPrice();
        ShareExchange shareExchange = new ShareExchange(UUID.randomUUID(), ExchangeType.BID, stockPrice, 100, Instant.now());
        this.mockMvc.perform(post(localUrl).contentType(MediaType.APPLICATION_JSON).content(asJsonString(shareExchange)))
                .andExpect(status().isCreated());
    }

    @Test
    public void postNewBidUnsuccessfully() throws Exception {
        ShareExchange shareExchange = new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 40, 100, Instant.now());
        this.mockMvc.perform(post(localUrl).contentType(MediaType.APPLICATION_JSON).content(asJsonString(shareExchange)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postNewEmptyOfferUnsuccessfully() throws Exception {
        ShareExchange shareExchange = new ShareExchange();
        this.mockMvc.perform(post(localUrl).contentType(MediaType.APPLICATION_JSON).content(asJsonString(shareExchange)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postNewNullOfferUnsuccessfully() throws Exception {
        this.mockMvc.perform(post(localUrl).contentType(MediaType.APPLICATION_JSON).content(asJsonString(null)))
                .andExpect(status().isBadRequest());
    }

    public static String asJsonString(final Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

