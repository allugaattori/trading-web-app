package com.group14.tradeapp.servicetest;

import com.group14.tradeapp.model.ShareExchange;
import com.group14.tradeapp.model.enums.ExchangeType;
import com.group14.tradeapp.model.enums.ValidatePrice;
import com.group14.tradeapp.service.ShareService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class ShareServiceTest {
    @Autowired
    private ShareService shareService;

    @Test
    public void saveNewShareExchangeSuccessfully() {
        shareService.setLatestPrice(200);
        ShareExchange shareExchange = new ShareExchange(UUID.randomUUID(), ExchangeType.OFFER, 200, 20, Instant.now());
        assertEquals(shareService.saveNewShareExchange(shareExchange), ValidatePrice.OK);
        shareExchange.setPrice(180);
        assertEquals(shareService.saveNewShareExchange(shareExchange), ValidatePrice.OK);
        shareExchange.setPrice(220);
        assertEquals(shareService.saveNewShareExchange(shareExchange), ValidatePrice.OK);
    }

    @Test
    public void saveNewShareExchangeUnsuccessfully() {
        shareService.setLatestPrice(200);
        ShareExchange shareExchange = new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 0, 0, Instant.now());
        assertEquals(shareService.saveNewShareExchange(shareExchange), ValidatePrice.TOO_LOW);
        shareExchange.setPrice(179);
        assertEquals(shareService.saveNewShareExchange(shareExchange), ValidatePrice.TOO_LOW);
        shareExchange.setPrice(221);
        assertEquals(shareService.saveNewShareExchange(shareExchange), ValidatePrice.TOO_HIGH);
    }

    @Test
    public void updateStockPriceSuccessfullyTest() {
        shareService.updateStockPrice();
        assertNotEquals(shareService.getLatestPrice(), -1.0);
        assertEquals(shareService.getLatestStockQuote().get("symbol").get(0).asText(), "AAPL");
    }

    @Test
    public void validateQuantitySuccessfullyAndUnsuccessfully() {
        assertTrue(shareService.validateQuantity(10));
        assertFalse(shareService.validateQuantity(-100));
        assertFalse(shareService.validateQuantity(0));
        assertFalse(shareService.validateQuantity(10.1));
    }

    @Test
    public void validateExchangeSuccessfullyTest() {
        double stockPrice = 200.00;
        assertEquals(shareService.validateExchange(new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 150, 1, Instant.now()), stockPrice),
                ValidatePrice.TOO_LOW);
        assertEquals(shareService.validateExchange(new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 179.99, 1, Instant.now()), stockPrice),
                ValidatePrice.TOO_LOW);
        assertEquals(shareService.validateExchange(new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 180, 1, Instant.now()), stockPrice),
                ValidatePrice.OK);
        assertEquals(shareService.validateExchange(new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 180.01, 1, Instant.now()), stockPrice),
                ValidatePrice.OK);
        assertEquals(shareService.validateExchange(new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 200, 1, Instant.now()), stockPrice),
                ValidatePrice.OK);
        assertEquals(shareService.validateExchange(new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 219.99, 1, Instant.now()), stockPrice),
                ValidatePrice.OK);
        assertEquals(shareService.validateExchange(new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 220, 1, Instant.now()), stockPrice),
                ValidatePrice.OK);
        assertEquals(shareService.validateExchange(new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 220.01, 1, Instant.now()), stockPrice),
                ValidatePrice.TOO_HIGH);
        assertEquals(shareService.validateExchange(new ShareExchange(UUID.randomUUID(), ExchangeType.BID, 300, 1, Instant.now()), stockPrice),
                ValidatePrice.TOO_HIGH);
    }

    @Test
    public void getUpdatedStockPriceSuccessfully() {
        shareService.setLatestPrice(0);
        double stockPrice = shareService.getLatestPrice();
        shareService.updateStockPrice();
        double updatedStockPrice = shareService.getLatestPrice();
        assertNotEquals(stockPrice, updatedStockPrice);
    }

}
