package com.group14.tradeapp.servicetest;

import com.group14.tradeapp.service.StockQuoteFetcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class StockQuoteFetcherTest {

    @Autowired
    private StockQuoteFetcher stockQuoteFetcher;

    @Test
    public void fetchStockQuoteSuccessfully() {
        String stockQuote = stockQuoteFetcher.fetch();
        assertNotNull(stockQuote);
    }

}