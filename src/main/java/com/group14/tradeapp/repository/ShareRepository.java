package com.group14.tradeapp.repository;

import com.group14.tradeapp.model.ShareExchange;
import jakarta.persistence.Id;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * In development
 * The interface handles the communication between this application and the database
 */
@Repository
public interface ShareRepository {
    ShareExchange findShareById(Id id);
    ArrayList<ShareExchange> findAll();
}
