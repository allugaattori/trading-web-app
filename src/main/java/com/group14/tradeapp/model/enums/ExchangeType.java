package com.group14.tradeapp.model.enums;

/**
 * Määritellään pyyntö- tai tarjous ENUM avulla. Täten ei tule ongelmia väärien Stringien kanssa.
 */
public enum ExchangeType {
    BID, OFFER
}
