package com.group14.tradeapp.model.enums;

public enum ValidatePrice {
    TOO_HIGH, TOO_LOW, OK
}
