package com.group14.tradeapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.group14.tradeapp.model.enums.ExchangeType;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ShareExchange {

    @Id
    @JsonIgnore
    private UUID id;
    @NotNull
    private ExchangeType exchangeType;
    @NotNull
    private double price;
    @NotNull
    private int quantity;
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private Instant timestamp;

}

