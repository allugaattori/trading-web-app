package com.group14.tradeapp.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Trade {

    @Id
    private UUID id;
    private Instant timestamp;
    private boolean price;
    private int quantity;
}
