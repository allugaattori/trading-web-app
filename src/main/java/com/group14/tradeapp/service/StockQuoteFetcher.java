package com.group14.tradeapp.service;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StockQuoteFetcher {
    private static final String url = "https://api.marketdata.app/v1/stocks/quotes/AAPL/";

    public String fetch() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url, String.class);
    }
}
