package com.group14.tradeapp.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.group14.tradeapp.model.ShareExchange;
import com.group14.tradeapp.model.enums.ValidatePrice;
import com.group14.tradeapp.repository.ShareRepository;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.UUID;

/**
 * Service does the work that requests handled by the controller requires
 */
@Service
@Getter
@Setter
public class ShareService {
    private JsonNode latestStockQuote;
    private double latestPrice;
    //Waiting for db implementation
    private ShareRepository shareRepository;

    @PostConstruct
    public void onInit() {
        updateStockPrice();
    }

    //Waiting for db implementation to go further
    public ValidatePrice saveNewShareExchange(ShareExchange shareExchangeRequest) {
        ValidatePrice isPriceValid = validateExchange(shareExchangeRequest, latestPrice);
        if (isPriceValid.equals(ValidatePrice.OK)) {
            shareExchangeRequest.setId(UUID.randomUUID());
            shareExchangeRequest.setTimestamp(Instant.now());
            System.out.println("service" + shareExchangeRequest);
        }
        return isPriceValid;
    }

    public ValidatePrice validateExchange(ShareExchange exchange, double latestKnownPrice) {
        if (exchange.getPrice() < 0.9 * latestKnownPrice) return ValidatePrice.TOO_LOW;
        if (1.1 * latestKnownPrice < exchange.getPrice()) return ValidatePrice.TOO_HIGH;
        return ValidatePrice.OK;
    }

    public boolean validateQuantity(double quantity) {
        if (quantity != (int)quantity) return false;
        return quantity > 0;
    }

    @Scheduled(cron = "@hourly")
    public void updateStockPrice() {
        StockQuoteFetcher fetcher = new StockQuoteFetcher();
        String stockString = fetcher.fetch();
        ObjectMapper mapper = new ObjectMapper();
        try {
            latestStockQuote = mapper.readTree(stockString);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }
        latestPrice = latestStockQuote.get("last").get(0).asDouble(-1.0);
    }

}

