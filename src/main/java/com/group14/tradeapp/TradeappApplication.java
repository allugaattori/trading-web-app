package com.group14.tradeapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TradeappApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeappApplication.class, args);
	}

}

