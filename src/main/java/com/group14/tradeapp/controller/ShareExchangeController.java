package com.group14.tradeapp.controller;

import com.group14.tradeapp.model.ShareExchange;
import com.group14.tradeapp.model.enums.ValidatePrice;
import com.group14.tradeapp.service.ShareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tradeapp/v1/")
public class ShareExchangeController {

    @Autowired
    private ShareService shareService;

    @GetMapping()
    private ResponseEntity<String> appInformation() {
        return new ResponseEntity<>("Welcome to Tradeapp! Please POST a BID/OFFER with price and quantity. " +
                "Latest trade price is: " + shareService.getLatestPrice(), HttpStatus.OK);
    }

    //TODO: change to return list of Trades
    @GetMapping("tradeinfo/")
    private ResponseEntity<String> tradeInformation() {
        return new ResponseEntity<>("Insert tradeinfo here", HttpStatus.OK);
    }

    @PostMapping()
    private ResponseEntity<String> postBidOrOffer(@Validated @RequestBody ShareExchange shareExchangeRequest) {
        if (!shareService.validateQuantity(shareExchangeRequest.getQuantity())) {
            return new ResponseEntity<>("Quantity is unacceptable. Please try another quantity.", HttpStatus.BAD_REQUEST);
        }
        ValidatePrice isPriceValid = shareService.saveNewShareExchange(shareExchangeRequest);
        if (isPriceValid.equals(ValidatePrice.TOO_HIGH)) {
            return new ResponseEntity<>("Transaction failed, the price specified was over 10% the last listed trade price.", HttpStatus.BAD_REQUEST);
        }
        if (isPriceValid.equals(ValidatePrice.TOO_LOW)) {
            return new ResponseEntity<>("Transaction failed, the price specified was under 10% the last listed trade price.", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Transaction created." + "\n" +
                "exchangeType: " + shareExchangeRequest.getExchangeType() + "\n" +
                "price: " + shareExchangeRequest.getPrice() + "\n" +
                "quantity: " + shareExchangeRequest.getQuantity(), HttpStatus.CREATED);
    }

}

