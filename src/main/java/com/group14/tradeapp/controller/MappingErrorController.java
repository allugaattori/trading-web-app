package com.group14.tradeapp.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

//TODO: Runtime error handling
@Controller
public class MappingErrorController implements ErrorController {

    //Whitelabel error page replacement
    @RequestMapping("/error")
    public ResponseEntity<String> handleError() {
        return new ResponseEntity<>("Whoops, there was something wrong with the request. " +
                "Check your URL and requestBody and try again.", HttpStatus.BAD_REQUEST);
    }

}
